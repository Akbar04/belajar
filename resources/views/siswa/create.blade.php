@extends('template')

@section('content')

<form action="{{url('siswa')}}" method="post">
    <label class="label">
        Tambah Siswa
    </label>
    {{ csrf_field() }}
    <div class="field">
      <label class="label" for="NISN">NISN</label>
      <div class="control">
        <input class="input" type="text" placeholder="Text input" name="NISN" id="NISN">
      </div>
    </div>

    <div class="field">
      <label class="label" for="nama">Nama</label>
      <div class="control">
        <input class="input" type="text" placeholder="Text input" name="nama" id="nama">
      </div>
    </div>

    <div class="field">
      <label class="label" for="tgl_lahir">Tgl Lahir</label>
      <div class="control">
        <input class="date" type="date" name="tgl_lahir" id="tgl_lahir">
      </div>
    </div>

    <div class="field">
        <label class="label" for="jenis_kelamin"> Jenis Kelamin:</label>
      <div class="control">
        <label class="radio">
          <input type="radio" name="jenis_kelamin" value="L" id="jenis_kelamin" >
          Laki-Laki
        </label>
        <label class="radio">
          <input type="radio" name="jenis_kelamin" value="P" id="jenis_kelamin">
          Perempuan
        </label>
      </div>
    </div>

      <div class="field is-grouped">
      <input type="submit" class="button is-link" value="Tambah">
      <div class="control">
        <button class="button is-text">Cancel</button>
      </div>
    </div>
</form>
@stop
