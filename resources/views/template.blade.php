<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script defer src="https://use.fontawesome.com/releases/v5.1.0/js/all.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/bulma.min.css') }}" rel="stylesheet">
</head>
<body>
      <nav class="navbar is-primary">
          <div class="navbar-brand">
              <a class="navbar-item" href="{{ url('/') }}">
                  {{ config('app.name', 'Laravel') }}
              </a>
              <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
                <span></span>
                <span></span>
                <span></spa

                n>
              </div>
            </div>
              <div class="navbar-menu" id="navbarExampleTransparentExample">
                  <!-- Left Side Of Navbar -->
                  <div class="navbar-start">
                    <a href="#" class="navbar-item">
                      home
                    </a>
                    <div class="navbar-item has-dropdown is-hoverable">
                      <a href="#" class="navbar-link">docs</a>
                      <div class="navbar-dropdown is-boxed">
                        <a href="#" class="navbar-item"></a>
                        <a href="#" class="navbar-item"></a>
                      </div>
                    </div>
                  </div>
                    <!-- Right Side Of Navbar -->
                  <div class="navbar-end">
                      <!-- Authentication Links -->
                      @guest
                        <a class="navbar-item" href="{{ route('login') }}">{{ __('Login') }}</a>
                        <a class="navbar-item" href="{{ route('register') }}">{{ __('Register') }}</a>
                      @else
                        <div class="navbar-item has-dropdown is-hoverable">
                          <a class="navbar-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                              <div class="navbar-dropdown is-boxed" aria-labelledby="navbarDropdown">
                                <a class="navbar-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                          </div>
                      @endguest
                  </div>
                </div>
      </nav>

        <main class="py-4">
            @yield('content')

            @include('footer')
        </main>
</body>
</html>
